The generated Facade.jar handles both encoding and decoding of data given a PNG and data file,
or encoded PNG respectively using a basic Java GUI.
Encoded and decoded files will appear in the same directory as selected image files.


Facade uses the least significant bits in RGB values of a PNG image to store a byte of data into
each pixel using pure Java features.

To have embedded data capacity near proportional to image dimensions, each pixel must represent a
byte of data, requiring 8 bits per pixel.  Even distribution across RGB results in 6 bits,
therefore 2 more bits from a channel can be utilized based on a checking method of previous pixels
to ensure extractability.  This results in an upper bound of 'height * width' bytes worth of data
in a given image:
  500 x 500 ~ 244.14063 KB
1200 x 1200 ~ 1.37329 MB
1920 x 1080 ~ 1.97754 MB

Measurement of data size is not exactly 'height * width' due to additional data required for direct
decoding without requiring additional assets.  Additional data stored within image includes:
offset                      = Spaces modified pixels out to keep as much of the original image as possible  (4 bytes)
data file extension length  = Length of data file's extension, 0 to 255  (1 byte)
data file extension         = Each character of data extension  (2 bytes per)
data size                   = Byte size of data file, allows 2 GB max  (4 bytes)

A debug file is generated alongside each encoded image with an extension '.png_debug'.
It is a valid PNG that, when opened, will show in red pixels the location of each modified pixel
excluding padding.


Demo: Includes a random image of 'landscape.png' which is used to store the 'Facade.jar' file in
the 'landscape_encoded.png' file.  The utilized pixels can be seen in 'landscape_encoded.png_debug'.
Successful decoding of 'landscape_encoded.png' will result in the same 'Facade.jar' file used to
encode it.