import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.util.Random;

public class Facade{
	private static final Dimension DIMENSION_BUTTON = new Dimension(400, new JButton("x").getPreferredSize().height);

	private static final Insets INSETS_10TLR_5B = new Insets(10, 10, 5, 10);
	private static final Insets INSETS_5TB_10LR = new Insets(5, 10, 5, 10);
	private static final Insets INSETS_5T_10LBR = new Insets(5, 10, 10, 10);

	private static final String TEXT_DATA_BUTTON = "Data File:  ";
	private static String TEXT_FILTER_DESCRIPTION;
	private static final String TEXT_IMAGE_BUTTON = "Image File:  ";
	private static final String TEXT_INFO_LABEL = "<html>%S<br>Pixels Needed:  %S<br>Example Sizes:  %S</html>";
	private static final String[] TEXT_MODE_OPTIONS = {"Encode Data", "Decode Image"};
	private static final String TEXT_OVERWRITE = "The file %S already exists!\nOverwrite anyway?";
	private static final String TEXT_TITLE = "Facade";

	private static final String TEXT_DOT = ".";
	private static final String TEXT_EMPTY = "";
	private static final String TEXT_REP = "%S";
	private static final String TEXT_SEP = " - ";

	private static final String USER_DIR = System.getProperty("user.dir");

	private static final String[] EXTENSIONS_IMAGES = {"png"};

	private static final FileFilter FILE_FILTER_IMAGES = new FileFilter(){
		@Override
		public boolean accept(File f){
			if(f.isDirectory()){
				return true;
			}
			String name = f.getName().toLowerCase();
			for(String str : EXTENSIONS_IMAGES){
				if(name.endsWith(TEXT_DOT+str)){
					return true;
				}
			}
			return false;
		}
		@Override
		public String getDescription(){
			return TEXT_FILTER_DESCRIPTION;
		}
	};

	/**
	 File that contains desired data.
	 */
	private static File dataFile;
	/**
	 Extension of desired data.
	 */
	private static String dataFileExtension;
	/**
	 Size of desired data.
	 */
	private static int dataSize;
	/**
	 Image that contains desired data.
	 */
	private static File dataImageFile;
	/**
	 Image that contains original image.
	 */
	private static File imageFile;
	/**
	 Raw image buffer.
	 */
	private static BufferedImage image;
	/**
	 Total amount of pixels needed in image.
	 */
	private static long neededSize;

	private static BufferedImage debugImage;
	private static Pixel debugPixel = new Pixel();
	private static boolean writeDebug = true;

	private static int x;
	private static int y;
	private static int offset;
	private static int width;
	private static int height;

	private static Pixel curPixel = new Pixel();
	private static Pixel checkPixel = new Pixel();

	private static String lastExtension = TEXT_EMPTY;

	public static void main(String[] args){
		StringBuilder filterDescription = new StringBuilder("Image File  (");
		for(String str : EXTENSIONS_IMAGES){
			filterDescription.append("*.").append(str).append(", ");
		}

		TEXT_FILTER_DESCRIPTION = filterDescription.substring(0, filterDescription.length() - 2)+")";

		displayModePrompt();
	}

	private static void displayModePrompt(){
		dataFile = null;
		dataFileExtension = null;
		dataImageFile = null;
		imageFile = null;
		image = null;

		JFrame frame = new JFrame(TEXT_TITLE);
		frame.setUndecorated(true);
		frame.setVisible(true);

		int choice = JOptionPane.showOptionDialog(null, "Choose mode:", TEXT_TITLE, JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, TEXT_MODE_OPTIONS, null);
		frame.dispose();
		if(choice == 0){
			displayEncodeMenu();
		}
		else if(choice == 1){
			displayDecodeMenu();
		}
	}

	private static void displayEncodeMenu(){
		String title = TEXT_TITLE+TEXT_SEP+TEXT_MODE_OPTIONS[0];

		JFrame frame =  new JFrame(title);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setLayout(new GridBagLayout());
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				frame.dispose();
				displayModePrompt();
			}
		});
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = INSETS_10TLR_5B;
		c.weightx = 1;
		c.weighty = 1;

		JButton dataButton = new JButton(TEXT_DATA_BUTTON);
		JButton imageButton = new JButton(TEXT_IMAGE_BUTTON);
		JLabel infoLabel = new JLabel(TEXT_INFO_LABEL.replace(TEXT_REP, TEXT_EMPTY), SwingConstants.LEFT);
		JButton encodeButton = new JButton("ENCODE");

		dataButton.setForeground(Color.BLACK);
		dataButton.setHorizontalAlignment(SwingConstants.LEFT);
		dataButton.setPreferredSize(new Dimension(DIMENSION_BUTTON));
		dataButton.setToolTipText("File to encode into an image");
		dataButton.addActionListener(e->{
			JFileChooser chooser = new JFileChooser(USER_DIR);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			if(chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
				File selectedFile = chooser.getSelectedFile();
				if(selectedFile != null && selectedFile.canRead()){
					dataFile = selectedFile;
					String fileName = selectedFile.getName();
					int index = fileName.lastIndexOf(TEXT_DOT);
					dataFileExtension = index > 0 ? fileName.substring(index + 1, fileName.length()) : TEXT_EMPTY;
					checkEncode(dataButton, imageButton, infoLabel, encodeButton);
				}
			}
		});
		frame.add(dataButton, c);

		imageButton.setForeground(Color.BLACK);
		imageButton.setHorizontalAlignment(SwingConstants.LEFT);
		imageButton.setToolTipText("Image to have the data encoded into");
		imageButton.addActionListener(e->{
			JFileChooser chooser = new JFileChooser(USER_DIR);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setFileFilter(FILE_FILTER_IMAGES);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			if(chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
				File selectedFile = chooser.getSelectedFile();
				if(selectedFile != null && selectedFile.canRead()){
					imageFile = selectedFile;
					image = null;
					checkEncode(dataButton, imageButton, infoLabel, encodeButton);
				}
			}
		});
		c.gridy++;
		c.insets = INSETS_5TB_10LR;
		frame.add(imageButton, c);

		infoLabel.setForeground(Color.BLACK);
		c.gridy++;
		frame.add(infoLabel, c);

		encodeButton.setEnabled(false);
		encodeButton.setForeground(Color.BLACK);
		encodeButton.addActionListener(e->{
			if(checkEncode(dataButton, imageButton, infoLabel, encodeButton)){
				if(encode(frame)){
					JOptionPane.showMessageDialog(frame, "Encoding successful!\n"+dataImageFile.getName(), title, JOptionPane.INFORMATION_MESSAGE);
				}
				else{
					JOptionPane.showMessageDialog(frame, "Failed to encode file!", title, JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		c.gridy++;
		c.insets = INSETS_5T_10LBR;
		frame.add(encodeButton, c);

		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private static void displayDecodeMenu(){
		String title = TEXT_TITLE+TEXT_SEP+TEXT_MODE_OPTIONS[1];

		JFrame frame =  new JFrame(title);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setLayout(new GridBagLayout());
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				frame.dispose();
				displayModePrompt();
			}
		});
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = INSETS_10TLR_5B;
		c.weightx = 1;
		c.weighty = 1;

		JButton imageButton = new JButton(TEXT_IMAGE_BUTTON);
		JLabel infoLabel = new JLabel(" ", SwingConstants.LEFT);
		JButton decodeButton = new JButton("DECODE");

		imageButton.setForeground(Color.BLACK);
		imageButton.setHorizontalAlignment(SwingConstants.LEFT);
		imageButton.setPreferredSize(new Dimension(DIMENSION_BUTTON));
		imageButton.setToolTipText("Image to decode into a file");
		imageButton.addActionListener(e->{
			JFileChooser chooser = new JFileChooser(USER_DIR);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setFileFilter(FILE_FILTER_IMAGES);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			if(chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
				File selectedFile = chooser.getSelectedFile();
				if(selectedFile != null && selectedFile.canRead()){
					dataImageFile = selectedFile;
					image = null;
					checkDecode(imageButton, infoLabel, decodeButton);
				}
			}
		});
		frame.add(imageButton, c);

		infoLabel.setForeground(Color.BLACK);
		c.gridy++;
		c.insets = INSETS_5TB_10LR;
		frame.add(infoLabel, c);

		decodeButton.setEnabled(false);
		decodeButton.setForeground(Color.BLACK);
		decodeButton.addActionListener(e->{
			if(checkDecode(imageButton, infoLabel, decodeButton)){
				if(decode(frame)){
					JOptionPane.showMessageDialog(frame, "Decoding successful!\n"+dataFile.getName(), title, JOptionPane.INFORMATION_MESSAGE);
				}
				else{
					JOptionPane.showMessageDialog(frame, "Failed to decode file!", title, JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		c.gridy++;
		c.insets = INSETS_5T_10LBR;
		frame.add(decodeButton, c);

		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private static boolean checkEncode(JButton dataButton, JButton imageButton, JLabel infoLabel, JButton encodeButton){
		dataButton.setForeground(Color.BLACK);
		dataButton.setText(TEXT_DATA_BUTTON+(dataFile == null ? TEXT_EMPTY : dataFile.getName()));
		imageButton.setForeground(Color.BLACK);
		imageButton.setText(TEXT_IMAGE_BUTTON+(imageFile == null ? TEXT_EMPTY : imageFile.getName()));
		infoLabel.setForeground(Color.BLACK);
		String infoLabelText = TEXT_INFO_LABEL;
		boolean pass = false;
		if(dataFile != null){
			if(dataFile.canRead()){
				if(dataFileExtension.length() < 256){
					dataSize = (int) dataFile.length();
					neededSize = dataSize + 4 + 1 + (dataFileExtension.length() * 2) + 4;
					if(imageFile == null){
						infoLabelText = infoLabelText.replaceFirst(TEXT_REP, TEXT_EMPTY);
					}
					else{
						if(imageFile.canRead()){
							try{
								image = image == null ? ImageIO.read(imageFile) : image;
								height = image.getHeight();
								width = image.getWidth();
								int imageSize = width * height;
								if(imageSize >= neededSize){
									infoLabelText = infoLabelText.replaceFirst(TEXT_REP, TEXT_EMPTY);
									pass = true;
								}
								else{
									imageButton.setForeground(Color.RED);
									infoLabel.setForeground(Color.RED);
									infoLabelText = infoLabelText.replaceFirst(TEXT_REP, "Image is not large enough!");
								}
							}catch(IOException e){
								imageButton.setForeground(Color.RED);
								infoLabel.setForeground(Color.RED);
								infoLabelText = infoLabelText.replaceFirst(TEXT_REP, "Image could not be loaded!");
							}
						}
						else{
							imageButton.setForeground(Color.RED);
							infoLabel.setForeground(Color.RED);
							infoLabelText = infoLabelText.replaceFirst(TEXT_REP, "Image could not be read!");
						}
					}
					int factor1 = (int) Math.ceil(Math.sqrt(neededSize));
					int factor2 = (int) Math.ceil(Math.sqrt(neededSize / (long) (4 * 3)));
					int factor3 = (int) Math.ceil(Math.sqrt(neededSize / (long) (16 * 9)));
					infoLabelText = infoLabelText.replaceFirst(TEXT_REP, TEXT_EMPTY+neededSize).replaceFirst(TEXT_REP, factor1 + " x " + factor1 + ",  " + (factor2 * 4) + " x " + (factor2 * 3) + ",  " + (factor3 * 16) + " x " + (factor3 * 9));
				}
				else{
					dataButton.setForeground(Color.RED);
					infoLabel.setForeground(Color.RED);
					infoLabelText = infoLabelText.replaceFirst(TEXT_REP, "Data file extension too long!");
				}
			}
			else{
				dataButton.setForeground(Color.RED);
				infoLabel.setForeground(Color.RED);
				infoLabelText = infoLabelText.replaceFirst(TEXT_REP, "Data file could not be read!");
			}
		}
		infoLabel.setText(infoLabelText.replace(TEXT_REP, TEXT_EMPTY));
		encodeButton.setEnabled(pass);
		return pass;
	}

	private static boolean checkDecode(JButton imageButton, JLabel infoLabel, JButton decodeButton){
		imageButton.setForeground(Color.BLACK);
		imageButton.setText(TEXT_IMAGE_BUTTON+(dataImageFile == null ? TEXT_EMPTY : dataImageFile.getName()));
		infoLabel.setForeground(Color.BLACK);
		String infoLabelText = " ";
		boolean pass = false;
		if(dataImageFile != null){
			if(dataImageFile.canRead()){
				try{
					image = image == null ? ImageIO.read(dataImageFile) : image;
					height = image.getHeight();
					width = image.getWidth();
					pass = true;
				}catch(IOException e){
					imageButton.setForeground(Color.RED);
					infoLabel.setForeground(Color.RED);
					infoLabelText = "Data image could not be loaded!";
				}
			}
			else{
				imageButton.setForeground(Color.RED);
				infoLabel.setForeground(Color.RED);
				infoLabelText = "Data image could not be read!";
			}
		}
		infoLabel.setText(infoLabelText);
		decodeButton.setEnabled(pass);
		return pass;
	}

	private static boolean encode(JFrame frame){
		dataImageFile = new File(createModifiedPath(imageFile, "_encoded", null));
		if(dataImageFile.exists() && JOptionPane.showConfirmDialog(frame, TEXT_OVERWRITE.replaceFirst(TEXT_REP, dataImageFile.getName()), TEXT_TITLE+TEXT_SEP+TEXT_MODE_OPTIONS[0], JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION){
			return false;
		}

		cloneDebug();

		offset = (int) (((((long) width * (long) height) - 4) / (neededSize - 4)) - 1);

		x = 0;
		y = 0;
		setPixelsValue();
		setEncode(offset);

		x = 1;
		fixPixelPosition();
		setPixelsValue();
		setEncode(offset >> 8);

		incrementPixelPosition();
		setEncode(dataFileExtension.length());

		int character;
		for(int i = 0; i < dataFileExtension.length(); i++){
			incrementPixelPosition();
			character = dataFileExtension.charAt(i);
			setEncode(character);
			incrementPixelPosition();
			setEncode(character >> 8);

		}

		incrementPixelPosition();
		setEncode(dataSize);
		incrementPixelPosition();
		setEncode(dataSize >> 8);
		incrementPixelPosition();
		setEncode(dataSize >> 16);
		incrementPixelPosition();
		setEncode(dataSize >> 24);

		try{
			FileInputStream inputStream = new FileInputStream(dataFile);
			int dataByte;
			while((dataByte = inputStream.read()) >= 0 && incrementPixelPosition()){
				setEncode(dataByte);
			}
			inputStream.close();
		}catch(IOException e){
			return false;
		}
		writeDebug = false;
		Random random = new Random(System.currentTimeMillis());
		while(incrementPixelPosition()){
			setEncode(random.nextInt());
		}
		writeDebug = true;

		x = width - 2;
		y = height - 1;
		fixPixelPosition();
		setPixelsValue();
		setEncode(offset >> 16);

		x++;
		fixPixelPosition();
		setPixelsValue();
		setEncode(offset >> 24);

		try{
			ImageIO.write(image, lastExtension.substring(1), dataImageFile);
			ImageIO.write(debugImage, lastExtension.substring(1), new File(dataImageFile.getAbsolutePath()+"_debug"));
		}catch(IOException e){
			return false;
		}

		return true;
	}

	private static boolean decode(JFrame frame){
		x = width - 1;
		y = height - 1;
		fixPixelPosition();
		setPixelsValue();
		offset = getDecode() << 8;
		x--;
		fixPixelPosition();
		setPixelsValue();
		offset = (offset | getDecode()) << 16;
		x = 0;
		y = 0;
		setPixelsValue();
		offset = offset | getDecode();
		x++;
		fixPixelPosition();
		setPixelsValue();
		offset = offset | (getDecode() << 8);

		incrementPixelPosition();
		int extensionLength = getDecode();

		int character;
		StringBuilder extension = new StringBuilder(extensionLength);
		for(int i = 0; i < extensionLength; i++){
			incrementPixelPosition();
			character = getDecode();
			incrementPixelPosition();
			character = character | (getDecode() << 8);
			extension.append((char) character);
		}
		dataFileExtension = extension.toString();

		incrementPixelPosition();
		dataSize = getDecode();
		incrementPixelPosition();
		dataSize = dataSize | (getDecode() << 8);
		incrementPixelPosition();
		dataSize = dataSize | (getDecode() << 16);
		incrementPixelPosition();
		dataSize = dataSize | (getDecode() << 24);

		dataFile = new File(createModifiedPath(dataImageFile, "_decoded", extensionLength > 0 ? TEXT_DOT+dataFileExtension : TEXT_EMPTY));
		if(dataFile.exists() && JOptionPane.showConfirmDialog(frame, TEXT_OVERWRITE.replaceFirst(TEXT_REP, dataFile.getName()), TEXT_TITLE+TEXT_SEP+TEXT_MODE_OPTIONS[1], JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION){
			return false;
		}

		try{
			FileOutputStream outputStream = new FileOutputStream(dataFile);
			while(dataSize > 0 && incrementPixelPosition()){
				outputStream.write(getDecode());
				dataSize--;
			}
			outputStream.close();
		}catch(IOException e){
			return false;
		}


		return true;
	}

	private static void setEncode(int data){
		curPixel.encodeByte(checkPixel, data);
		image.setRGB(x, y, curPixel.value());

		if(writeDebug){
			debugPixel.set(curPixel.value());
			debugPixel.val[0] = 255;
			debugPixel.val[1] = 0;
			debugPixel.val[2] = 0;
			debugImage.setRGB(x, y, debugPixel.value());
		}
	}

	private static int getDecode(){
		return curPixel.decodeByte(checkPixel);
	}

	private static void setPixelsValue(){
		curPixel.set(image.getRGB(x, y));
		if(x - 1 == -1){
			if(y - 1 == -1){
				checkPixel.reset();
			}
			else{
				checkPixel.set(image.getRGB(width - 1, y - 1));
			}
		}
		else{
			checkPixel.set(image.getRGB(x - 1, y));
		}
	}

	private static void fixPixelPosition(){
		if(x < 0){
			x = 0;
			y--;
		}
		while(x >= width){
			x -= width;
			y++;
		}
	}

	private static boolean incrementPixelPosition(){
		x += offset + 1;
		fixPixelPosition();
		if(y > height - 1 || (y == height - 1 && x >= width - 1)){
			return false;
		}
		setPixelsValue();
		return true;
	}

	private static String createModifiedPath(File originalFile, String ending, String customExtension){
		String originalPath = originalFile.getAbsolutePath();
		int index = originalPath.lastIndexOf(TEXT_DOT);
		lastExtension = customExtension == null ? originalPath.substring(index) : customExtension;
		return originalPath.substring(0, index)+ending+lastExtension;
	}


	private static void cloneDebug(){
		ColorModel colorModel = image.getColorModel();
		debugImage = new BufferedImage(colorModel, image.copyData(null), colorModel.isAlphaPremultiplied(), null);
	}
}