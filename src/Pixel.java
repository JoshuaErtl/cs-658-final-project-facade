class Pixel{
	int[] val = {0, 0, 0, 0}; // r, g, b, a

	void set(int rgba){
		val[3] = (rgba >> 24) & 0xFF;
		val[0] = (rgba >> 16) & 0xFF;
		val[1] = (rgba >> 8) & 0xFF;
		val[2] = rgba & 0xFF;
	}

	int value(){
		return (val[3] << 24) | (val[0] << 16) | (val[1] << 8) | val[2];
	}

	private int getMinIndex(){
		int index = 0;
		int min = val[0];
		for(int i = 1; i < 3; i++){
			if(min > val[i]){
				index = i;
				min = val[i];
			}
		}
		return index;
	}

	void encodeByte(Pixel checkPixel, int data){
		for(int i = 0; i < 3; i++){
			val[i] = (val[i] & 0xFC) | (data & 0x3);
			data = data >> 2;
		}
		int index = checkPixel.getMinIndex();
		val[index] = (val[index] & 0xF3) | ((data & 0x3) << 2);
	}

	int decodeByte(Pixel checkPixel){
		int data = (val[checkPixel.getMinIndex()] >> 2) & 0x3;
		for(int i = 2; i >= 0; i--){
			data = (data << 2) | (val[i] & 0x3);
		}
		return data;
	}

	void reset(){
		for(int i = 0; i < 4; i++){
			val[i] = 0;
		}
	}
}